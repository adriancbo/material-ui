'use strict';

var React = require('react/addons');

module.exports = {
  componentWillMount: function componentWillMount() {
    this.intervals = [];
  },
  componentDidMount: function componentDidMount() {
    if (this.state.rstId === 2) {
      var positions = this._setInitialPositionState();
      var selectedIndex = this.state.selectedIndex;
      this.setState({
        positions: positions,
        selectedIndex: selectedIndex,
        left: positions[selectedIndex].left,
        width: positions[selectedIndex].width
      });
    }
  },
  componentWillReceiveProps: function componentWillReceiveProps(newProps) {
    if (this.state.rstId === 1) {
      this._sizeChecking(newProps.left);
    }
  },
  componentWillUnmount: function componentWillUnmount() {
    this.intervals.map(clearInterval);
  },
  setInterval: (function (_setInterval) {
    function setInterval() {
      return _setInterval.apply(this, arguments);
    }

    setInterval.toString = function () {
      return _setInterval.toString();
    };

    return setInterval;
  })(function () {
    this.intervals.push(setInterval.apply(null, arguments));
  }),
  _setInitialPositionState: function _setInitialPositionState() {
    var positions = {};
    var tabs = this.refs;
    for (var tab in tabs) {
      var node = React.findDOMNode(tabs[tab]);
      var rect = node.getBoundingClientRect();
      positions[tab] = {
        left: node.offsetLeft,
        width: parseInt(rect.width, 10)
      };
    }
    return positions;
  }

};