/**
 * ReactScrollTabs
 *
 * Based on JQUERY SCROLL TABS v2.0 by Josh Reed
 *
 * Extends some React methods to easily and cleanly make use of ScrollTabs.
 *
 *
 * Usage: ReactScrollTabs.findDOMNode(document.getElementById('YourReactComponentId')).scrollTabs();
 *
 * Version:   0.1
 * Author:    Adrian Carballo
 */

'use strict';

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var React = require('react');
// const $ = require('jquery');
var Dom = require('../utils/dom');
var Events = require('../utils/events');
var ImmutabilityHelper = require('../utils/immutability-helper');
var ReactScrollTabsMixin = require('./ReactScrollTabsMixin');
var shallowEqual = require('../utils/shallow-equal');
var StylePropable = require('../mixins/style-propable');
var tweenState = require('react-tween-state');
var update = React.addons.update;

var scrollSelectedIntoView = function scrollSelectedIntoView(left, leftInner) {
  this._tweenState("leftInner", false, left, leftInner);
};
var slideToActive = function slideToActive(left, handleSlide) {
  var _getDOMCache = this.getDOMCache();

  var tabs = _getDOMCache.tabs;
  var firstTab = _getDOMCache.firstTab;
  var lastTab = _getDOMCache.lastTab;
  var leftFinisher = _getDOMCache.leftFinisher;
  var rightFinisher = _getDOMCache.rightFinisher;
  var prevSelectedTab = _getDOMCache.prevSelectedTab;

  var tabSelected = tabs[this.props.selectedIndex];

  if (handleSlide !== false) {
    var val = this.getVals();
    if (typeof left !== "number") {
      left = tabSelected.offsetLeft;
    }

    var leftNotVisible = val.leftInner + tabSelected.offsetWidth > left + tabSelected.offsetWidth + 5;
    var rightNotVisible = val.outerWidth + val.leftInner - (left + tabSelected.offsetWidth) < 0;
    if (leftNotVisible || rightNotVisible) {
      // "Slide" it into view if not fully visible.
      scrollSelectedIntoView.call(this, left, val.leftInner);
    }
  }
  if (prevSelectedTab) {
    prevSelectedTab.classList.remove("tab_selected", "scroll_tab_first_selected", "scroll_tab_last_selected", "scroll_tab_l_finisher_selected", "scroll_tab_r_finisher_selected");
  }
  Dom.addClass(tabSelected, "tab_selected");
  if (Dom.hasClass(tabSelected, 'scroll_tab_l_finisher')) {
    firstTab.classList.add("tab_selected", "scroll_tab_first_selected");
  }
  if (Dom.hasClass(tabSelected, 'scroll_tab_r_finisher')) {
    lastTab.classList.add("tab_selected", "scroll_tab_last_selected");
  }
  if (Dom.hasClass(tabSelected, 'scroll_tab_first') || Dom.hasClass(lastTab, 'scroll_tab_first')) {
    leftFinisher.classList.add("tab_selected", "scroll_tab_l_finisher_selected");
  }
  if (Dom.hasClass(tabSelected, 'scroll_tab_last') || Dom.hasClass(firstTab, 'scroll_tab_last')) {
    rightFinisher.classList.add("tab_selected", "scroll_tab_l_finisher_selected");
  }
  this.setDOMCache({ prevSelectedTab: tabSelected });
};

var ReactScrollTabs = React.createClass({
  displayName: 'ReactScrollTabs',

  mixins: [StylePropable, ReactScrollTabsMixin, tweenState.Mixin],

  componentWillMount: function componentWillMount() {
    this.domCache = {};
    this.setState({
      arrowbtn: false,
      finisherbtn: false,
      leftInner: 0,
      rstId: 1,
      init: true
    });
  },

  componentDidMount: function componentDidMount() {
    var cached = this.setDOMCache({
      scrollTabInnerEl: React.findDOMNode(this.refs.scroll_tab_inner)
    });
    this._initDOM(cached.scrollTabInnerEl);
    this.setInterval(this._sizeChecking.bind(this, false, false), 400);
  },

  componentWillUnmount: function componentWillUnmount() {
    this.domCache = null;
  },

  shouldComponentUpdate: function shouldComponentUpdate(nextProps, nextState) {
    var tabEqual = nextProps.selectedIndex === this.props.selectedIndex;
    var tweenEqual = nextState.leftInner === this.getTweeningValue('leftInner');
    return !tabEqual || !tweenEqual || !shallowEqual(this.state, nextState);
  },

  componentDidUpdate: function componentDidUpdate(prevProps, prevState) {
    if (this.state.init) this.setState({ init: false });
  },

  getVals: function getVals() {
    var scrollTabInnerEl = this.getDOMCache("scrollTabInnerEl");
    // scrollWith is total inner width (including whats hidden)
    // offsetWidth, clientWith are visible inner width
    return {
      leftInner: scrollTabInnerEl.scrollLeft,
      outerWidth: scrollTabInnerEl.offsetWidth,
      panelWidth: scrollTabInnerEl.offsetWidth,
      scrollWidth: scrollTabInnerEl.scrollWidth,
      scrollTabInnerEl: scrollTabInnerEl
    };
  },

  getStyles: function getStyles(themeVariables) {
    return {
      wrapper: {
        width: '100%',
        boxShadow: 'none',
        display: 'flex',
        minHeight: '48px',
        maxHeight: '50%',
        flexShrink: '0',
        alignItems: 'flex-start',
        position: 'relative',
        backgroundColor: themeVariables.tabs.backgroundColor,
        color: themeVariables.tabs.color || 'rgba(255,255,255,1)'
      },
      scrollTabInner: {
        margin: '0px',
        overflow: 'hidden',
        whiteSpace: 'nowrap',
        msTextOverflow: 'clip',
        textOverflow: 'clip',
        fontSize: '0px',
        position: 'static',
        overflowX: 'auto',
        overflowY: 'hidden'
      }
    };
  },

  getClassNames: function getClassNames() {
    var _this = this;

    if (arguments.length === 0) return null;
    var types = Array.prototype.slice.call(arguments, 0);

    var builder = function builder(type, direction) {
      var out = undefined;
      switch (type) {
        case "arrowbtn":
          var stateProp = direction === "l" ? _this.state.arrowbtn_l_Off : _this.state.arrowbtn_r_Off;
          out = '\n            scroll_tab_' + direction + '_button\n            material-icons\n            scrollindicator\n            scrollindicator--' + direction;
          out += _this.state.arrowbtn ? "" : " hidden";
          out += stateProp ? '\n            scroll_arrow_disabled\n            scroll_tab_' + direction + '_button_disabled' : "";
          break;
        case "finisherbtn":
          out = 'scroll_tab_' + direction + '_finisher';
          out += _this.state.finisherbtn ? "" : " hidden";
          break;
        default:
      }
      return out;
    };

    var results = types.map(function (className) {
      return _defineProperty({}, className, {
        l: builder(className, "l"),
        r: builder(className, "r")
      });
    });
    return ImmutabilityHelper.merge.apply(this, results);
  },

  render: function render() {
    var _props = this.props;
    var style = _props.style;
    var themeVariables = _props.themeVariables;

    var styles = this.getStyles(themeVariables);
    var css = this.getClassNames.apply(this, ["arrowbtn", "finisherbtn"]);

    if (this.domCache.scrollTabInnerEl) {
      if (this.getTweeningValue('leftInner') !== this.state.leftInner || this.state.init) {
        this.domCache.scrollTabInnerEl.scrollLeft = this.getTweeningValue('leftInner');
      }
    }

    return React.createElement(
      'div',
      { className: 'scroll_tabs_wrapper', style: this.mergeAndPrefix(styles.wrapper, style) },
      React.createElement(
        'i',
        { onTouchTap: this._handleScrollIndicatorTap,
          className: css.arrowbtn.l },
        'keyboard_arrow_left'
      ),
      React.createElement(
        'div',
        { className: 'scroll_tabs_container' },
        React.createElement(
          'div',
          { ref: 'scroll_tab_inner', className: 'scroll_tab_inner', style: styles.scrollTabInner },
          React.createElement(
            'span',
            { className: css.finisherbtn.l },
            ' '
          ),
          this._getTabs(),
          React.createElement(
            'span',
            { className: css.finisherbtn.r },
            ' '
          )
        )
      ),
      React.createElement(
        'i',
        { onTouchTap: this._handleScrollIndicatorTap,
          className: css.arrowbtn.r },
        'keyboard_arrow_right'
      )
    );
  },

  _initDOM: function _initDOM(scrollTabInnerEl) {
    var dom = {},
        finishers = [];
    var children = scrollTabInnerEl.children;
    var childrenArr = Array.prototype.slice.call(children, 0);

    var tabs = childrenArr.filter(function (el, i) {
      if (children[i].tagName === "SPAN") {
        var test = !children[i].classList.contains("scroll_tab_l_finisher") && !children[i].classList.contains("scroll_tab_r_finisher");
        if (!test) {
          finishers.push(children[i]);
        }
        return test;
      }
    });

    dom.children = children;
    dom.tabs = tabs;
    dom.firstTab = tabs[0];
    dom.lastTab = tabs[tabs.length - 1];
    dom.leftFinisher = finishers[0];
    dom.rightFinisher = finishers[1];

    dom = update(dom, { $merge: dom });

    Dom.addClass(dom.firstTab, "scroll_tab_first");
    Dom.addClass(dom.lastTab, "scroll_tab_last");

    // Check to set the edges as selected if needed
    if (Dom.hasClass(dom.firstTab, 'tab_selected')) {
      dom.leftFinisher.classList.add("tab_selected", "scroll_tab_l_finisher_selected");
    }

    if (Dom.hasClass(dom.lastTab, 'tab_selected')) {
      dom.rightFinisher.classList.add("tab_selected", "scroll_tab_r_finisher_selected");
    }
    // cache dom object of initialized nodes via the mixin
    this.setDOMCache(dom);
  },

  _sizeChecking: function _sizeChecking(left, handleSlide) {
    var domCache = this.getDOMCache("scrollTabInnerEl", "tabs");
    var scrollTabInnerEl = domCache.scrollTabInnerEl;
    var panelWidth = scrollTabInnerEl.offsetWidth;
    var scrollWidth = scrollTabInnerEl.scrollWidth;

    if (scrollWidth > panelWidth) {
      this.setState({
        arrowbtn: true,
        finisherbtn: false
      });
      var scrollLeft = scrollTabInnerEl.scrollLeft;
      if (scrollWidth - panelWidth - 5 <= scrollLeft) {
        this.setState({ arrowbtn_r_Off: true });
      } else {
        this.setState({ arrowbtn_r_Off: false });
      }

      if (scrollLeft === 0) {
        this.setState({ arrowbtn_l_Off: true });
      } else {
        this.setState({ arrowbtn_l_Off: false });
      }

      slideToActive.call(this, left, handleSlide);
    } else {
      var tabsCount = domCache.tabs.length;
      this.setState({
        arrowbtn: false,
        finisherbtn: tabsCount > 0 ? false : true
      });
    }
  },

  _getTabs: function _getTabs() {
    return React.Children.map(this.props.children, function (tabs) {
      return React.Children.map(tabs.props.children, function (tab, index) {
        if (tab !== null) {
          return tab;
        }
      });
    });
  },

  _handleScrollIndicatorTap: function _handleScrollIndicatorTap(e) {
    var _this2 = this;

    e.stopPropagation();
    var scrollDistance = this.props.scrollerConfig.scrollDistance;

    if (Dom.hasClass(e.nativeEvent.target, "scroll_tab_r_button")) {
      var scrollRightFunc = function scrollRightFunc() {
        var val = _this2.getVals();
        var beginVal = val.leftInner;
        var endVal = val.leftInner + scrollDistance;
        if (_this2.state.leftInner === val.leftInner + scrollDistance) {
          return false;
        }
        _this2._tweenState("leftInner", false, endVal, beginVal);
      };
      scrollRightFunc();
    } else {
      var scrollLeftFunc = function scrollLeftFunc() {
        var val = _this2.getVals();
        var beginVal = val.leftInner;
        var endVal = val.leftInner - scrollDistance;
        if (-endVal === scrollDistance) {
          return false;
        }
        _this2._tweenState("leftInner", false, endVal, beginVal);
      };
      scrollLeftFunc();
    }
  },

  _tweenState: function _tweenState(prop, duration, endVal, beginVal) {
    var _this3 = this;

    var scrollDuration = this.props.scrollerConfig.scrollDuration;

    if (beginVal <= 1 && endVal <= 1) return;
    this.tweenState(prop, {
      easing: tweenState.easingTypes.easeInOutQuad,
      duration: duration || scrollDuration,
      beginValue: !this.state.init ? beginVal : endVal,
      endValue: endVal,
      onEnd: function onEnd() {
        _this3._sizeChecking(null, false);
      }
    });
  },

  setDOMCache: function setDOMCache(obj) {
    this.domCache = update(this.domCache, { $merge: obj });
    return this.domCache;
  },

  getDOMCache: function getDOMCache() {
    if (arguments.length === 0) return this.domCache;
    var args = Array.prototype.slice.call(arguments, 0);

    if (args.length === 1) return this.domCache[args[0]];

    var newObj = {};
    for (var i = 0; i < args.length; i++) {
      if (args[i]) {
        newObj = update(newObj, { $merge: _defineProperty({}, args[i], this.domCache[args[i]]) });
      }
    }
    return newObj;
  }

});

module.exports = ReactScrollTabs;