/**
 * ReactScrollTabs
 *
 * Based on JQUERY SCROLL TABS v2.0 by Josh Reed
 *
 * Extends some React methods to easily and cleanly make use of ScrollTabs.
 *
 *
 * Usage: ReactScrollTabs.findDOMNode(document.getElementById('YourReactComponentId')).scrollTabs();
 *
 * Version:   0.1
 * Author:    Adrian Carballo
 */

'use strict';

const React = require('react');
// const $ = require('jquery');
const Dom = require('../utils/dom');
const Events = require('../utils/events');
const ImmutabilityHelper = require('../utils/immutability-helper');
const ReactScrollTabsMixin = require('./ReactScrollTabsMixin');
const shallowEqual = require('../utils/shallow-equal');
const StylePropable = require('../mixins/style-propable');
const tweenState = require('react-tween-state');
const update = React.addons.update;

const scrollSelectedIntoView = function (left, leftInner) {
  this._tweenState("leftInner", false, left, leftInner);
};
const slideToActive = function(left, handleSlide) {
  let {
    tabs,
    firstTab,
    lastTab,
    leftFinisher,
    rightFinisher,
    prevSelectedTab,
  } = this.getDOMCache();

  let tabSelected = tabs[this.props.selectedIndex];

  if (handleSlide !== false) {
    const val = this.getVals();
    if (typeof left !== "number") { left = tabSelected.offsetLeft; }

    const leftNotVisible = (val.leftInner + tabSelected.offsetWidth) > (left + tabSelected.offsetWidth + 5);
    const rightNotVisible = (((val.outerWidth + val.leftInner) - (left + tabSelected.offsetWidth)) < 0);
    if ((leftNotVisible || rightNotVisible)) {
      // "Slide" it into view if not fully visible.
      scrollSelectedIntoView.call(this, left, val.leftInner);
    }
  }
  if (prevSelectedTab) {
    prevSelectedTab.classList.remove("tab_selected", "scroll_tab_first_selected", "scroll_tab_last_selected", "scroll_tab_l_finisher_selected", "scroll_tab_r_finisher_selected");
  }
  Dom.addClass(tabSelected, "tab_selected");
  if (Dom.hasClass(tabSelected, 'scroll_tab_l_finisher')) {
    firstTab.classList.add("tab_selected", "scroll_tab_first_selected");
  }
  if (Dom.hasClass(tabSelected, 'scroll_tab_r_finisher')) {
    lastTab.classList.add("tab_selected", "scroll_tab_last_selected");
  }
  if (Dom.hasClass(tabSelected, 'scroll_tab_first') ||
      Dom.hasClass(lastTab, 'scroll_tab_first')) {
    leftFinisher.classList.add("tab_selected", "scroll_tab_l_finisher_selected");
  }
  if (Dom.hasClass(tabSelected, 'scroll_tab_last') || Dom.hasClass(firstTab, 'scroll_tab_last')) {
    rightFinisher.classList.add("tab_selected", "scroll_tab_l_finisher_selected");
  }
  this.setDOMCache({prevSelectedTab: tabSelected});
};

const ReactScrollTabs = React.createClass({
  mixins: [StylePropable, ReactScrollTabsMixin, tweenState.Mixin],

  componentWillMount() {
    this.domCache = {};
    this.setState({
      arrowbtn: false,
      finisherbtn: false,
      leftInner: 0,
      rstId: 1,
      init: true,
    });
  },

  componentDidMount() {
    let cached = this.setDOMCache({
      scrollTabInnerEl: React.findDOMNode(this.refs.scroll_tab_inner),
    });
    this._initDOM(cached.scrollTabInnerEl);
    this.setInterval(this._sizeChecking.bind(this, false, false), 400);
  },

  componentWillUnmount() {
    this.domCache = null;
  },

  shouldComponentUpdate(nextProps, nextState) {
    let tabEqual = nextProps.selectedIndex === this.props.selectedIndex;
    let tweenEqual = nextState.leftInner === this.getTweeningValue('leftInner');
    return (
      !tabEqual ||
      !tweenEqual ||
      !shallowEqual(this.state, nextState)
    );
  },

  componentDidUpdate(prevProps, prevState) {
    if (this.state.init) this.setState({init: false});
  },

  getVals() {
    let scrollTabInnerEl = this.getDOMCache("scrollTabInnerEl");
    // scrollWith is total inner width (including whats hidden)
    // offsetWidth, clientWith are visible inner width
    return {
      leftInner: scrollTabInnerEl.scrollLeft,
      outerWidth: scrollTabInnerEl.offsetWidth,
      panelWidth: scrollTabInnerEl.offsetWidth,
      scrollWidth: scrollTabInnerEl.scrollWidth,
      scrollTabInnerEl: scrollTabInnerEl,
    }
  },

  getStyles(themeVariables) {
    return {
      wrapper: {
        width: '100%',
        boxShadow: 'none',
        display: 'flex',
        minHeight: '48px',
        maxHeight: '50%',
        flexShrink: '0',
        alignItems: 'flex-start',
        position: 'relative',
        backgroundColor: themeVariables.tabs.backgroundColor,
        color: themeVariables.tabs.color || 'rgba(255,255,255,1)',
      },
      scrollTabInner: {
        margin: '0px',
        overflow: 'hidden',
        whiteSpace: 'nowrap',
        msTextOverflow: 'clip',
        textOverflow: 'clip',
        fontSize: '0px',
        position: 'static',
        overflowX: 'auto',
        overflowY: 'hidden',
      },
    }
  },

  getClassNames() {
    if (arguments.length === 0) return null;
    const types = Array.prototype.slice.call(arguments, 0);

    let builder = (type, direction) => {
      let out;
      switch (type) {
        case "arrowbtn":
          let stateProp = direction === "l" ?
            this.state.arrowbtn_l_Off :
            this.state.arrowbtn_r_Off;
          out = `
            scroll_tab_${direction}_button
            material-icons
            scrollindicator
            scrollindicator--${direction}`;
          out += this.state.arrowbtn ? "" : " hidden";
          out += stateProp ? `
            scroll_arrow_disabled
            scroll_tab_${direction}_button_disabled` : "";
          break;
        case "finisherbtn":
          out = `scroll_tab_${direction}_finisher`;
          out += this.state.finisherbtn ? "" : " hidden";
          break;
        default:
      }
      return out;
    }

    let results = types.map((className) => {
      return {
        [className]: {
          l: builder(className, "l"),
          r: builder(className, "r"),
        },
      }
    });
    return ImmutabilityHelper.merge.apply(this, results);
  },

  render() {
    let {
      style,
      themeVariables,
    } = this.props;

    let styles = this.getStyles(themeVariables);
    let css = this.getClassNames.apply(this, ["arrowbtn", "finisherbtn"]);

    if (this.domCache.scrollTabInnerEl) {
      if ((this.getTweeningValue('leftInner') !== this.state.leftInner) || this.state.init) {
        this.domCache.scrollTabInnerEl.scrollLeft = this.getTweeningValue('leftInner');
      }
    }

    return (
      <div className="scroll_tabs_wrapper" style={this.mergeAndPrefix(styles.wrapper, style)}>
        <i onTouchTap={this._handleScrollIndicatorTap}
          className={css.arrowbtn.l}>keyboard_arrow_left</i>
        <div className="scroll_tabs_container">
          <div ref="scroll_tab_inner" className="scroll_tab_inner" style={styles.scrollTabInner}>
            <span className={css.finisherbtn.l}>&nbsp;</span>
              {this._getTabs()}
            <span className={css.finisherbtn.r}>&nbsp;</span>
          </div>
        </div>
        <i onTouchTap={this._handleScrollIndicatorTap}
          className={css.arrowbtn.r}>keyboard_arrow_right</i>
      </div>
    );
  },

  _initDOM(scrollTabInnerEl) {
    let dom = {}, finishers = [];
    let children = scrollTabInnerEl.children;
    let childrenArr = Array.prototype.slice.call(children, 0);

    let tabs = childrenArr.filter((el, i) => {
      if (children[i].tagName === "SPAN") {
        let test = (
          !children[i].classList.contains("scroll_tab_l_finisher") &&
          !children[i].classList.contains("scroll_tab_r_finisher")
        );
        if (!test) {
          finishers.push(children[i]);
        }
        return test;
      }
    });

    dom.children = children;
    dom.tabs = tabs;
    dom.firstTab = tabs[0];
    dom.lastTab = tabs[tabs.length - 1];
    dom.leftFinisher = finishers[0];
    dom.rightFinisher = finishers[1];

    dom = update(dom, {$merge: dom});

    Dom.addClass(dom.firstTab, "scroll_tab_first");
    Dom.addClass(dom.lastTab, "scroll_tab_last");

    // Check to set the edges as selected if needed
    if (Dom.hasClass(dom.firstTab, 'tab_selected')) {
      dom.leftFinisher.classList.add("tab_selected", "scroll_tab_l_finisher_selected");
    }

    if (Dom.hasClass(dom.lastTab, 'tab_selected')) {
      dom.rightFinisher.classList.add("tab_selected", "scroll_tab_r_finisher_selected");
    }
    // cache dom object of initialized nodes via the mixin
    this.setDOMCache(dom);
  },

  _sizeChecking(left, handleSlide) {
    let domCache = this.getDOMCache("scrollTabInnerEl", "tabs");
    let scrollTabInnerEl = domCache.scrollTabInnerEl;
    let panelWidth = scrollTabInnerEl.offsetWidth;
    let scrollWidth = scrollTabInnerEl.scrollWidth;

    if (scrollWidth > panelWidth) {
      this.setState({
        arrowbtn: true,
        finisherbtn: false,
      });
      let scrollLeft = scrollTabInnerEl.scrollLeft;
      if ((scrollWidth - panelWidth - 5) <= (scrollLeft)) {
        this.setState({arrowbtn_r_Off: true});
      } else {
        this.setState({arrowbtn_r_Off: false});
      }

      if (scrollLeft === 0) {
        this.setState({arrowbtn_l_Off: true});
      } else {
        this.setState({arrowbtn_l_Off: false});
      }

      slideToActive.call(this, left, handleSlide);

    } else {
      let tabsCount = domCache.tabs.length;
      this.setState({
        arrowbtn: false,
        finisherbtn: tabsCount > 0 ? false : true,
      });
    }
  },

  _getTabs() {
    return React.Children.map(this.props.children, (tabs) => {
      return React.Children.map(tabs.props.children, (tab, index) => {
        if (tab !== null) {
          return tab;
        }
      });
    });
  },

  _handleScrollIndicatorTap(e) {
    e.stopPropagation();
    const {scrollDistance} = this.props.scrollerConfig;

    if (Dom.hasClass(e.nativeEvent.target, "scroll_tab_r_button")) {
      let scrollRightFunc = () => {
        const val = this.getVals();
        let beginVal = val.leftInner;
        let endVal = val.leftInner + scrollDistance;
        if (this.state.leftInner === (val.leftInner + scrollDistance)) {
          return false;
        }
        this._tweenState("leftInner", false, endVal, beginVal);
      }
      scrollRightFunc();
    } else {
      let scrollLeftFunc = () => {
        const val = this.getVals();
        let beginVal = val.leftInner;
        let endVal = val.leftInner - scrollDistance;
        if (-(endVal) === scrollDistance) {
          return false;
        }
        this._tweenState("leftInner", false, endVal, beginVal);
      }
      scrollLeftFunc();
    }
  },

  _tweenState(prop, duration, endVal, beginVal) {
    let {
      scrollDuration,
    } = this.props.scrollerConfig;

    if (beginVal <= 1 && endVal <= 1) return;
    this.tweenState(prop, {
      easing: tweenState.easingTypes.easeInOutQuad,
      duration: duration || scrollDuration,
      beginValue: !this.state.init ? beginVal : endVal,
      endValue: endVal,
      onEnd: () => { this._sizeChecking(null, false) },
    });
  },

  setDOMCache: function(obj) {
    this.domCache = update(this.domCache, {$merge: obj});
    return this.domCache;
  },

  getDOMCache: function() {
    if (arguments.length === 0) return this.domCache;
    const args = Array.prototype.slice.call(arguments, 0);

    if (args.length === 1) return this.domCache[args[0]];

    let newObj = {};
    for (let i = 0; i < args.length; i++) {
      if (args[i]) {
        newObj = update(newObj, {$merge: { [args[i]]: this.domCache[args[i]]}});
      }
    }
    return newObj;
  },

});

module.exports = ReactScrollTabs;
