const React = require('react/addons');

module.exports = {
  componentWillMount() {
    this.intervals = [];
  },
  componentDidMount() {
    if (this.state.rstId === 2) {
      let positions = this._setInitialPositionState();
      let selectedIndex = this.state.selectedIndex;
      this.setState({
        positions: positions,
        selectedIndex: selectedIndex,
        left: positions[selectedIndex].left,
        width: positions[selectedIndex].width,
      });
    }
  },
  componentWillReceiveProps(newProps) {
    if (this.state.rstId === 1) {
      this._sizeChecking(newProps.left);
    }
  },
  componentWillUnmount: function() {
    this.intervals.map(clearInterval);
  },
  setInterval: function() {
    this.intervals.push(setInterval.apply(null, arguments));
  },
  _setInitialPositionState() {
    let positions = {};
    let tabs = this.refs;
    for (let tab in tabs) {
      let node = React.findDOMNode(tabs[tab]);
      let rect = node.getBoundingClientRect();
      positions[tab] = {
        left: node.offsetLeft,
        width: parseInt(rect.width, 10),
      };
    }
    return positions;
  },

};
