let React = require('react/addons');
let TabScrollableTemplate = require('./tabTemplate');
let InkBar = require('../ink-bar');
let StylePropable = require('../mixins/style-propable');
let Controllable = require('../mixins/controllable');
let ReactScrollTabsMixin = require('./ReactScrollTabsMixin');
let ReactScrollTabs = require('./ReactScrollTabs');

let TabsScrollable = React.createClass({

  mixins: [StylePropable, Controllable, ReactScrollTabsMixin],

  contextTypes: {
    muiTheme: React.PropTypes.object,
  },

 propTypes: {
    contentContainerStyle: React.PropTypes.object,
    initialSelectedIndex: React.PropTypes.number,
    inkBarStyle: React.PropTypes.object,
    tabItemContainerStyle: React.PropTypes.object,
  },

  getDefaultProps() {
    return {
      initialSelectedIndex : 0,
    };
  },

  getInitialState(){
    let valueLink = this.getValueLink(this.props);
    let initialIndex = this.props.initialSelectedIndex;

    return {
      selectedIndex: valueLink.value ?
        this._getSelectedIndex(this.props) :
        initialIndex < this.getTabCount() ?
        initialIndex :
        0,
      left: '',
      width: '',
      positions: {},
      rstId: 2,
    };
  },

  getEvenWidth(){
    return (
      parseInt(window
        .getComputedStyle(React.findDOMNode(this))
        .getPropertyValue('width'), 10)
    );
  },

  getTabCount() {
    return React.Children.count(this.props.children);
  },

  componentWillReceiveProps(newProps) {
    let valueLink = this.getValueLink(newProps);

    if (valueLink.value){
      let selectedIndex = this._getSelectedIndex(newProps);
      this.setState({
        selectedIndex: selectedIndex,
      });
    }
  },

  shouldComponentUpdate(nextProps, nextState) {
    let tabWidthsReady = Object.keys(this.state.positions).length !== 0;

    if ( !tabWidthsReady ) { return true }

    return (nextState.selectedIndex !== this.state.selectedIndex);
  },

  render() {
    let {
      children,
      contentContainerStyle,
      initialSelectedIndex,
      inkBarStyle,
      style,
      tabWidth,
      tabItemContainerStyle,
      scrollerConfig,
      ...other,
    } = this.props;

    let themeVariables = this.context.muiTheme.component;
    let valueLink = this.getValueLink(this.props);
    let tabValue = valueLink.value;
    let tabContent = [];
    let width, left;

    let tabWidthsReady = Object.keys(this.state.positions).length !== 0;
    let positions = tabWidthsReady ? this.state.positions : null;

    if (this.state.selectedIndex !== -1) {
      width = positions ?
        positions[this.state.selectedIndex].width + 'px' :
        this.state.width + 'px';
      left = positions ?
        positions[this.state.selectedIndex].left :
        this.state.left;
    }
    // width = 100 / this.getTabCount() +'%';
    // left = 'calc(' + width + '*' + this.state.selectedIndex + ')';

    let tabs = React.Children.map(children, (tab, index) => {
      if (tab.type.displayName === "TabScrollable") {
        if (!tab.props.value && tabValue && process.env.NODE_ENV !== 'production') {
          console.error('Tabs value prop has been passed, but Tab ' + index +
          ' does not have a value prop. Needs value if Tabs is going' +
          ' to be a controlled component.');
        }

        tabContent.push(tab.props.children ?
          React.createElement(TabScrollableTemplate, {
            key: index,
            selected: this._getSelected(tab, index),
          }, tab.props.children) : undefined);

        return React.cloneElement(tab, {
          key: index,
          ref: index,
          selected: this._getSelected(tab, index),
          tabIndex: index,
          width: positions ? positions[index].width + 'px' : 'auto',
          onTouchTap: this._handleTabTouchTap,
        });
      }
      else {
        let type = tab.type.displayName || tab.type;
        console.error('Tabs only accepts Tab Components as children. Found ' +
              type + ' as child number ' + (index + 1) + ' of Tabs');
      }
    }, this);

    let inkBar = (this.state.selectedIndex !== -1) ? (
      <InkBar
        left={left + 'px'}
        width={width}
        style={inkBarStyle}/>
    ) : null;

    let inkBarContainerWidth = tabItemContainerStyle ?
      tabItemContainerStyle.width : '100%';

    return (
      <ReactScrollTabs
        {...other}
        left={left}
        selectedIndex={this.state.selectedIndex}
        scrollerConfig={scrollerConfig}
        themeVariables={themeVariables}
        style={this.mergeAndPrefix(style)}>

        <div style={this.mergeAndPrefix(tabItemContainerStyle)}>
            {tabs}
          <div style={{width: inkBarContainerWidth}}>
            {inkBar}
          </div>
        </div>
        <div style={this.mergeAndPrefix(contentContainerStyle)}>
          {tabContent}
        </div>

      </ReactScrollTabs>
    );
  },

  _getSelectedIndex(props) {
    let valueLink = this.getValueLink(props);
    let selectedIndex = -1;

    React.Children.forEach(props.children, (tab, index) => {
      if (valueLink.value === tab.props.value) {
        selectedIndex = index;
      }
    });

    return selectedIndex;
  },

  _handleTabTouchTap(value, e, tab){
    let valueLink = this.getValueLink(this.props);
    let tabIndex = tab.props.tabIndex;

    if ((valueLink.value && valueLink.value !== value) ||
      this.state.selectedIndex !== tabIndex) {
      valueLink.requestChange(value, e, tab);
    }

    this.setState({selectedIndex: tabIndex});

    tab.props.onActive(tab);
  },

  _getSelected(tab, index) {
    let valueLink = this.getValueLink(this.props);
    return valueLink.value ? valueLink.value === tab.props.value :
      this.state.selectedIndex === index;
  },

});

module.exports = TabsScrollable;

