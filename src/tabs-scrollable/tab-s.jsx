let React = require('react');
let StylePropable = require('../mixins/style-propable');

let TabScrollable = React.createClass({

  mixins: [StylePropable],

  contextTypes: {
    muiTheme: React.PropTypes.object,
  },

  propTypes: {
    onTouchTap: React.PropTypes.func,
    label: React.PropTypes.string,
    onActive: React.PropTypes.func,
    selected: React.PropTypes.bool,
    width: React.PropTypes.string,
    value: React.PropTypes.string,
  },

  getDefaultProps(){
    return {
      onActive: () => {},
      onTouchTap: () => {},
    };
  },

  render() {
    let {
      label,
      onActive,
      onTouchTap,
      selected,
      style,
      value,
      width,
      ...other,
    } = this.props;

    let styles = this.mergeAndPrefix({
      display: 'inline-block',
      cursor: 'pointer',
      textAlign: 'center',
      verticalAlign: 'middle',
      height: 48,
      lineHeight: '48px',
      color: selected ? 'rgba(255,255,255,1)' : 'rgba(255,255,255,0.6)',
      outline: 'none',
      fontSize: 14,
      fontWeight: 500,
      whiteSpace: 'nowrap',
      fontFamily: this.context.muiTheme.contentFontFamily,
      boxSizing: 'border-box',
      paddingLeft: 26,
      paddingRight: 26,
      width: width,

    }, this.props.style);

    return (
      <span
        {...other}
        style={styles}
        onTouchTap={this._handleTouchTap}>
        {label}
      </span>
    );

  },

  _handleTouchTap(e) {
    this.props.onTouchTap(this.props.value, e, this);
  },

});

module.exports = TabScrollable;
