let React = require('react');
let Router = require('react-router');
let CodeExample = require('../../code-example/code-example');
let {Styles, TabScrollable, TabsScrollable } = require('material-ui');
let ComponentDoc = require('../../component-doc');

let RouteHandler = Router.RouteHandler;
let { Colors, Spacing, Typography } = Styles;
let ThemeManager = new Styles.ThemeManager();

class TabsScrollablePage extends React.Component {

  constructor() {
    super();
  }

  componentWillMount(){
    this.setState({tabIndex: this._getSelectedIndex()});
    let setTabsState = function() {
      this.setState({renderTabs: !(document.body.clientWidth <= 647)});
    }.bind(this);
    setTabsState();
    window.onresize = setTabsState;
  }

  componentWillReceiveProps() {
    this.setState({tabIndex: this._getSelectedIndex()});
  }

  render(){
    let title =
      this.context.router.isActive('/components/tabs-custom/1') ? 'tabs-custom/1' :
      this.context.router.isActive('/components/tabs-custom/2') ? 'tabs-custom/2' :
      this.context.router.isActive('/components/tabs-custom/3') ? 'tabs-custom/3' :
      this.context.router.isActive('/components/tabs-custom/4') ? 'tabs-custom/4' : '';

    return (
      <div>
        {this._getTabs()}
        <h1>{title}</h1>
        <RouteHandler />
      </div>
    );
  }

  _getTabs() {
    let padding = 400;
    let styles = {
      container: {
        position: 'absolute',
        right: (Spacing.desktopGutter/2) + 48,
        bottom: 0,
      },
      tabsContainer: {
        position: 'relative',
        paddingLeft: padding,
      },
    };

    let scrollerConfig = {
      scrollDistance: 300,
      scrollDuration: 300,
      leftArrowSize: 48,
      rightArrowSize: 48,
      initialScrollPos: 0,
    };

    return(
      <div>
        <TabsScrollable
          scrollerConfig={scrollerConfig}
          style={styles.tabs}
          value={this.state.tabIndex}
          onChange={this._handleTabChange.bind(this)}>
          <TabScrollable
            value="1"
            label="TAB 1"
            style={styles.tab}
            route="/components/tabs-custom/1" />
          <TabScrollable
            value="2"
            label="TAB 2 IS LONGER"
            style={styles.tab}
            route="/components/tabs-custom/2"/>
          <TabScrollable
            value="3"
            label="TAB 3 IS EVEN LONGER"
            style={styles.tab}
            route="/components/tabs-custom/3"/>
          <TabScrollable
            value="4"
            label="THERES EVEN A FOURTH TAB"
            style={styles.tab}
            route="/components/tabs-custom/4"/>
        </TabsScrollable>
      </div>
    );
  }

  _getSelectedIndex() {
    return this.context.router.isActive('/components/tabs-custom/1') ? '1' :
      this.context.router.isActive('/components/tabs-custom/2') ? '2' :
      this.context.router.isActive('/components/tabs-custom/3') ? '3' :
      this.context.router.isActive('/components/tabs-custom/4') ? '4' : '0';
  }

  _handleTabChange(value, e, tab) {
    this.context.router.transitionTo(tab.props.route);
    this.setState({tabIndex: this._getSelectedIndex()});
  }

  _handleButtonClick() {
    this.setState({tabsValue: 'c'});
  }

  _handleTabsChange(value, e, tab){
    this.setState({tabsValue: value});
  }
};

TabsScrollablePage.contextTypes = {
  router: React.PropTypes.func
};
module.exports = TabsScrollablePage;
